# coding: utf-8

# In[41]:


import numpy as np
import soundfile as sf # install this in the kernel under Settings
import os
import keras
import pandas as pd
from keras.callbacks import TensorBoard
from keras.models import Sequential
from keras.layers import Dense, Activation
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.model_selection import cross_val_predict
from sklearn.metrics import confusion_matrix



# from python_speech_features import mfcc
# from python_speech_features import delta
# from python_speech_features import logfbank


# In[42]:


def scan_folder(parent,x):
    count = 0
    for file_name in os.listdir(parent):
        if file_name.endswith(".flac"):
            file_path = parent+"/"+file_name
            x.append(file_path)            
        else:
            current_path = "".join((parent,"/",file_name))
            if os.path.isdir(current_path):
                scan_folder(current_path,x)

def join_result(old_array, new_array):
    variabel = ','.join(old_array)
    arraybaru = variabel.split(',')
    
    for i in arraybaru:
        data, samplerate = sf.read(i)
        new_array.append(data[0:1000])
        

        
    


# In[43]:


# seed = 7
# np.random.seed(seed)

x = []
x2 = []
y = []
y2 = []
dataset = []
scan_folder("data",x)
scan_folder("asr_sundanese",x2)

print(x)
print(x2)

#convert and insert to one array
join_result(x,dataset)
join_result(x2,dataset)

y = np.ones(len(x)).tolist()
y2 = np.zeros(len(x2)).tolist()
y.extend(y2)
print(dataset)
X = np.array(dataset)
print(X)
y = keras.utils.to_categorical(y, num_classes=2)
x_train, x_test, y_train, y_test = train_test_split(X, y, test_size=0.1, random_state=42)


# In[50]:


model = Sequential()
model.add(Dense(1024, activation='relu', input_shape=X[0].shape))
model.add(Dense(512, activation='relu'))
model.add(Dense(256, activation='relu'))
model.add(Dense(128, activation='relu'))
model.add(Dense(64, activation='relu'))
model.add(Dense(32, activation='relu'))
model.add(Dense(16, activation='relu'))
model.add(Dense(8, activation='relu'))
model.add(Dense(2, activation='sigmoid'))
model.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy'])

tbCallBack = keras.callbacks.TensorBoard(log_dir='./logs', histogram_freq=0, batch_size=32, write_graph=True, write_grads=False, 
	write_images=False, embeddings_freq=0, embeddings_layer_names=None, embeddings_metadata=None, embeddings_data=None)
history = model.fit(x_train, y_train, epochs=100, batch_size=32,verbose = 1, validation_data=(x_test,y_test), callbacks=[tbCallBack])

print("On valid data")
pred1=model.evaluate(x_test,y_test)
print("Accuracy", str(pred1[1]*100))
print("Total loss",str(pred1[0]*100))
print("On training data")
pred1=model.evaluate(x_train,y_train)
print("Accuracy", str(pred1[1]*100))
print("Total loss",str(pred1[0]*100))


# In[46]:


loss = history.history['loss']
val_loss = history.history['val_loss']
epochs = range(1, len(loss) + 1)
plt.plot(epochs, loss, color='red', label='Training loss')
plt.plot(epochs, val_loss, color='green', label='Validation loss')
plt.title('Training and validation loss')
plt.xlabel('Epochs')
plt.ylabel('Loss')
plt.legend()
plt.show()


# In[47]:


acc = history.history['acc']
val_acc = history.history['val_acc']
plt.plot(epochs, acc, color='red', label='Training acc')
plt.plot(epochs, val_acc, color='green', label='Validation acc')
plt.title('Training and validation accuracy')
plt.xlabel('Epochs')
plt.ylabel('Loss')
plt.legend()
plt.show()


ypred = model.predict(x_test)
matrix = confusion_matrix(y_test.argmax(axis=1), ypred.argmax(axis=1))
sns.heatmap(matrix, annot = True,  linewidths=.5, cbar =None)
plt.ylabel('True label')
plt.xlabel('Predicted label')
plt.title('Confusion matrix')
plt.show()

